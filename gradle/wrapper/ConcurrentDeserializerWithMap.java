package com.gomul;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;


//implementation with relaxed requirement  :
// * Each returned instance will probably be retained on the heap for a long time.
//also assumes that number of consumers is fixed so the algorithm is still wait-free due to lack of collisions
public class ConcurrentDeserializerWithMap {
    AtomicReference<StampedValue<byte[]>> latestBlob = new AtomicReference<>(null);

    Map<Long,StampedValue<WeakReference<Object>>> valuesPerThread = new ConcurrentHashMap<>();
    /**
     * Sets a new serialized value. Exclusively called by a single producer
     * thread.
     */
    public void setSerialized(byte[] blob) {
        StampedValue<byte[]> previousBlob = latestBlob.get();
        long timestamp = getTimestamp(previousBlob);
        latestBlob.set(new StampedValue<>(blob, timestamp));
    }

    private long getTimestamp(StampedValue<byte[]> previousBlob) {
        if (previousBlob != null) {
            return previousBlob.timestamp + 1;
        }
        return 1;
    }

    /**
     * Returns the result of deserializing a blob previously set by the
     * producer thread. Called by arbitrarily many consumer threads. Initially
     * (before the first invocation of {@link #setSerialized(byte[])}) the
     * method returns {@code null}.
     */
    public Object getDeserialized() {
        StampedValue<byte[]> value = this.latestBlob.getAndSet(null); //makes sure value is computed once
        if (value != null) {
            processBlob(value);
        }
        StampedValue<WeakReference<Object>> newest = new StampedValue<>(new WeakReference<>(null),0);

        for (StampedValue<WeakReference<Object>> objectStampedValue : valuesPerThread.values()) {

            if(objectStampedValue.value.get()!=null && newest.timestamp<objectStampedValue.timestamp){
                newest = objectStampedValue;
            }
        }

        return newest.value.get();
    }



    private void processBlob(StampedValue<byte[]> toDeserializeBlob) {
        StampedValue<WeakReference<Object>> deserializedObject = deserialize(toDeserializeBlob);
        long id = Thread.currentThread().getId();
        valuesPerThread.put(id,deserializedObject);
    }


    private StampedValue<WeakReference<Object>> deserialize(StampedValue<byte[]> toDeserialize) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Object deserialized = mockDeserializeAlgorithm(toDeserialize.value);//possibly long running
        return new StampedValue<>(new WeakReference<>(deserialized), toDeserialize.timestamp);
    }


    // Details of deserialization are out of scope for this assignment.
    // You may use this mock implementation:
    private Object mockDeserializeAlgorithm(byte[] blob) {
        return new String(blob);
    }

    private class StampedValue<T> {
        final long timestamp;
        final T value;

        public StampedValue(T value, long timestamp) {
            this.value = value;
            this.timestamp = timestamp;
        }
    }
}
