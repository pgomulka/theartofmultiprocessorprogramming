package com.gomul.ch7.spinlocks;

import org.hamcrest.CoreMatchers;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class LockTests {

    private Lock lock;

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> data() {
        return List.of(new Object[][]{
                {new CLHLock()},
                {new MCSLock()},
                {new AndersonLock(3)},
        });
    }

    public LockTests(Lock lock) {
        this.lock = lock;
    }

    @Test
    public void shouldExecuteLoopWithoutInterruptionOfOtherThreads() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        List<Integer> result = new ArrayList<>();
        executorService.submit(appendIdTask(lock, 1, latch, result));
        executorService.submit(appendIdTask(lock, 2, latch, result));

        latch.countDown();

        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
        assertThat(result, is(grouped()));
    }

    class Wrapper {
        int counter = 0;
    }

    @Test
    public void shouldIncrementCounter() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        int numberOfIncrements = 10;

        Wrapper wrapper = new Wrapper();
        executorService.submit(incrementCounterTask(lock, numberOfIncrements, latch, wrapper));
        executorService.submit(incrementCounterTask(lock, numberOfIncrements, latch, wrapper));

        latch.countDown();

        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        assertThat(wrapper.counter, is(equalTo(20)));
    }

    private Runnable incrementCounterTask(Lock lock, int numberOfIncrements, CountDownLatch latch, Wrapper wrapper) {
        return () -> {
            try {
                latch.await();
                for (int i = 0; i < numberOfIncrements; i++) {
                    lock.lock();
                    try {
                        wrapper.counter = wrapper.counter + 1;
                    } finally {
                        lock.unlock();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
    }


    private <T> Matcher<List<T>> grouped() {
        return new FeatureMatcher<List<T>, Boolean>(CoreMatchers.equalTo(true), "isGrouped", "isGrouped") {
            @Override
            protected Boolean featureValueOf(List<T> ts) {
                return isGrouped(ts);
            }
        };
    }


    @Test
    public void t1() {
        assertTrue(isGrouped(List.of(1, 2, 3)));
        assertTrue(isGrouped(List.of(1, 1, 2, 3)));
        assertTrue(isGrouped(List.of(1, 1, 2, 2, 3, 3)));
        assertTrue(isGrouped(List.of(1, 1, 1, 1)));

        assertFalse(isGrouped(List.of(1, 2, 1)));
        assertFalse(isGrouped(List.of(1, 2, 1, 2)));
        assertFalse(isGrouped(List.of(1, 1, 2, 1)));
    }

    private static <T> boolean isGrouped(List<T> list) {
        T prev = null;
        Set<T> visited = new HashSet<>();
        for (T i : list) {
            if (prev != null && i != prev && visited.contains(i)) {
                return false;
            }
            prev = i;
            visited.add(i);
        }
        return true;
    }

    private Runnable appendIdTask(Lock lock, Integer id, CountDownLatch latch, List<Integer> list) {
        return () -> {
            try {
                latch.await();
                lock.lock();
                for (int j = 0; j < 10; j++)
                    list.add(id);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        };
    }

    private void lockPrintUnlock(Lock lock) {

    }
}