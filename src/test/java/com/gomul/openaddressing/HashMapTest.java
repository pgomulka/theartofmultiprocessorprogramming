package com.gomul.openaddressing;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class HashMapTest {
    @Test
    public void setEqualsGet() {
        int n = 2<<100;
        List<Integer> numbers = IntStream.range(0, n*2)
                .boxed()
                .collect(Collectors.toList());
        Collections.shuffle(numbers);
        List<Integer> integers = numbers.subList(0, n-1);

        HashMap<Integer, Integer> hashMap = new HashMap<>(n);
        for (int nu : integers) {
            hashMap.put(nu, nu);
        }
        for (Integer nu : integers) {
            Assert.assertEquals(hashMap.get(nu),nu);
        }
    }

    @Test
    public void cacheMissesLookup() throws IOException {
        int n = 2 << 1000;
        HashMap<Integer, Integer> hashMap = new HashMap<>(n);
        List<Integer> numbers = IntStream.range(0, n*2)
                .boxed()
                .collect(Collectors.toList());
        Collections.shuffle(numbers);
        List<Integer> last10 = new LinkedList<>();
        int sum = 0;
        List<Integer> integers = numbers.subList(0, n-1);

        BufferedWriter writer = new BufferedWriter(new FileWriter("data"));
        for (int nu : integers) {
            hashMap.put(nu, nu);
            if(last10.size()<10){
                last10.add(hashMap.lastCacheMissesCount());
                sum += hashMap.lastCacheMissesCount();
            }else{
                Integer remove = last10.remove(0);
                last10.add(hashMap.lastCacheMissesCount());
                sum+=hashMap.lastCacheMissesCount();
                sum-=remove;
            }
            writer.append(String.format("%.12f %.12f",hashMap.loadFactor(),((double)sum/last10.size())));
            writer.append("\n");
        }
        writer.close();

        Runtime commandPrompt = Runtime.getRuntime();
//        commandPrompt.exec("gnuplot -e \"plot 'data'; pause -1\"");
//        commandPrompt.();
    }

    @Test
    public void testLookupSpeed() {
        int n = 2 << 1000;
        warmup(100000);
        long start = System.nanoTime();
        HashMap<Integer, Integer> hashMap = new HashMap<>(n);
        for (int i = 0; i < n; i++) {
            hashMap.put(i, i);
        }
        long end = System.nanoTime();
        System.out.println(end - start);
    }

    private void warmup(int n) {
        HashMap<Integer, Integer> hashMap = new HashMap<>(n);
        for (int i = 0; i < n; i++) {
            hashMap.put(i, i);
        }
    }
}