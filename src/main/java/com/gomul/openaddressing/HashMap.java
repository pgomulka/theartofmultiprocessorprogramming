package com.gomul.openaddressing;

public class HashMap<K, V> implements Map<K, V> {

    int n;
    private final Entry<K, V>[] tab;
    private int numOfElements = 0;
    private int lastCacheMissesCount = 0;

    public HashMap(int n) {
        this.n = n;
        this.tab = new Entry[n];
    }

    public HashMap() {
        this(32);
    }

    private int findSlot(K key) {
        int misses = 0;
        int i = key.hashCode() % n;
        while (tab[i]!=null && !key.equals(tab[i].key)) {
            i = (i + 1) %n;
            misses++;
        }
        lastCacheMissesCount = misses;
        return i;
    }


    @Override
    public void put(K key, V value) {
        int slot = findSlot(key);
        tab[slot] = new Entry<>();
        tab[slot].value = value;
        tab[slot].key = key;
        numOfElements++;
    }

    @Override
    public V get(K key) {
        int slot = findSlot(key);
        if (tab[slot]!=null) {
            return tab[slot].value;
        }
        return null;
    }

    public double loadFactor() {
        return (double)numOfElements/n;
    }

    public int lastCacheMissesCount() {
        return lastCacheMissesCount;
    }


    static class Entry<K, V> {
        K key;
        V value;
    }
}
