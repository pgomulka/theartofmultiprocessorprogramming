package com.gomul.ch1.ex1;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class DiningPhilosopher {
    static volatile boolean running = true;
    static final CountDownLatch startLatch = new CountDownLatch(1);

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        Semaphore[] chopsticks = new Semaphore[5];
        for (int i = 0; i < 5; i++) {
            final int n = i;
            chopsticks[i] = new Semaphore(1) {
                @Override
                public String toString() {
                    return super.toString() + " n=" + n;
                }
            };
        }
        Philosopher[] philosophers = new Philosopher[5];
        for (int i = 0; i < 5; i++) {
            Semaphore left = chopsticks[i];
            Semaphore right = chopsticks[(i + 1) % 5];
            if ((i + 1) == 5) {

                philosophers[i] = new Philosopher(i, right, left);
            } else {

                philosophers[i] = new Philosopher(i, left, right);
            }
            executorService.submit(philosophers[i]);
        }
        startLatch.countDown();
        Thread.sleep(10000);
        running = false;
    }

    static class Philosopher implements Runnable {
        private final int id;
        private final Semaphore left;
        private final Semaphore right;
        private final Random random = new Random(System.currentTimeMillis());

        public Philosopher(int id, Semaphore left, Semaphore right) {
            this.id = id;
            this.left = left;
            this.right = right;
        }

        @Override
        public void run() {
//            try {
//                startLatch.await();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            while (running) {

                try {
                    left.acquire();
                    System.out.println(String.format("%s philosopher %d taken left", new Date(), id, left.toString()));

                    Thread.sleep(1000);
                    try {
                        right.acquire();
                        System.out.println(String.format("%s philosopher %d taken right", new Date(), id, right.toString()));

                        int eatingTime = random.nextInt(30);
                        System.out.println(String.format("%s philosopher %d eating %d", new Date(), id, eatingTime));
                        Thread.sleep(eatingTime * 100);
                        System.out.println(String.format("%s philosopher %d finished eating", new Date(), id));

                    } finally {
                        right.release();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    left.release();
                }

                int thinkingTime = random.nextInt(30);
                System.out.println(String.format("%s philosopher %d thinking %d", new Date(), id, thinkingTime));
                try {
                    Thread.sleep(thinkingTime * 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(String.format("%s philosopher %d finished thinking", new Date(), id));

            }
        }
    }
}
