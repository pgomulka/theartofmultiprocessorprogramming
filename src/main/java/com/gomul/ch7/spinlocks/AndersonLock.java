package com.gomul.ch7.spinlocks;


import java.util.concurrent.atomic.AtomicInteger;

/**
 * cons: requires an array, each thread spins on its own location, but it will most likely be in the same cache line
 * therefore if modified one entry, it will invalidate whole line
 * could be fixed with padding with each element of array being in separate cache line
 *
 */
public class AndersonLock implements Lock {

    private ThreadLocal<Integer> mySlot = ThreadLocal.withInitial(() -> 0); // local therefore no cache traffic

    private volatile boolean[] queue; // volatile due to avoiding optimisations in while loop. Not introducing a barrier
    private final int maxThreadCount;
    private final AtomicInteger tail;

    public AndersonLock(int maxThreadCount) {
        this.maxThreadCount = maxThreadCount;
        this.queue = new boolean[maxThreadCount];
        this.queue[0] = true;
        this.tail = new AtomicInteger(0);
    }

    @Override
    public void lock() {
        int slot = tail.getAndIncrement() % maxThreadCount;
        mySlot.set(slot);
        //now this get propagated thanks to cache coherency protocol
        while (!queue[slot]) {
        }
    }

    @Override
    public void unlock() {
        Integer slot = mySlot.get();
        queue[slot] = false;
        queue[(slot + 1) % maxThreadCount] = true;
    }
}
