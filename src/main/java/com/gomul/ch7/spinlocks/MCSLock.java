package com.gomul.ch7.spinlocks;

import java.util.concurrent.atomic.AtomicReference;

/**
 * The MCSLock, due to John Mellor-Crummey and Michael Scott [113],
 * 113. Mellor-Crummey J, Scott ML.
 * Algorithms for scalable synchronization on shared-memory multiprocessors.
 * ACM Transactions on Computer Systems. 1991;9(1):21–65.
 *
 * Today’s Java Virtual Machines use object synchronization based on simplified monitor algorithms such as
 * the Thinlock of David Bacon, Ravi Konuru, Chet Murthy, and Mauricio Serrano [17],
 * the Metalock of Ole Agesen, Dave Detlefs, Alex Garthwaite, Ross Knippel, Y. S. Ramakrishna and Derek White [7], or
 * the RelaxedLock of Dave Dice [31].
 * All these algorithms are variations of the MCSLock lock.
 */
public class MCSLock implements Lock {
    static class QNode {
        QNode next;
        boolean locked;
    }

    AtomicReference<QNode> tail = new AtomicReference<>();
    ThreadLocal<QNode> myNode = ThreadLocal.withInitial(() -> new QNode());

    @Override
    public void lock() {
        QNode node = myNode.get();
        QNode prev = tail.getAndSet(node);
        if (prev != null) {
            node.locked = true;
            prev.next = node;
            while (node.locked) {
            }
        }
    }

    @Override
    public void unlock() {
        QNode node = myNode.get();
        if (node.next == null) { // either the only element in a list, or waiting the other thread is slow with constructing the link
            if (tail.compareAndSet(node, null)) { // checks if the only thread in the list, otherwise return
                return;
            }
            while (node.next == null) {//wait for another thred to construct the link
            }
        }
        node.next.locked = false;
        node.next = null;
    }
}
