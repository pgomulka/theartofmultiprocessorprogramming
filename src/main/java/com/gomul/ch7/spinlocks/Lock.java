package com.gomul.ch7.spinlocks;

public interface Lock {

    void lock();
    void unlock();
}
