package com.gomul.ch7.spinlocks;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Travis Craig, Erik Hagersten, and Anders Landin
 * <p>
 * <p>
 * 30. T. Craig. Building FIFO and priority-queueing spin locks from atomic swap.
 * Technical Report TR 93-02-02, University of Washington, Department of Computer Science, February 1993.
 * <p>
 * 110. P. Magnussen, A. Landin, and E. Hagersten.
 * Queue locks on cache coherent multiprocessors.
 * In Proc. of the Eighth International Symposium on Parallel Processing (IPPS),
 * pp. 165–171, April 1994. IEEE Computer Society, April 1994. Vancouver,
 * British Columbia, Canada, NY, USA, 1987, ACM Press.
 *
 * cons: spins on a prev node. If that is on a remote location in NUMA architecture, this would be expensive. See MCSLock how to avoid that
 * pros: each thread spins on its own location, memory complexity is O(n) n number of locks
 */
public class CLHLock implements Lock {

    static class QNode {
        boolean locked = false;
    }

    private AtomicReference<QNode> tail = new AtomicReference<>();
    private ThreadLocal<QNode> myPred = ThreadLocal.withInitial(() -> null);
    private ThreadLocal<QNode> myNode = ThreadLocal.withInitial(() -> new QNode());

    @Override
    public void lock() {
        QNode qNode = myNode.get();
        qNode.locked = true;
        QNode prev = tail.getAndSet(qNode);
        myPred.set(prev);
        while (prev.locked) {
        }
    }

    @Override
    public void unlock() {
        QNode qNode = myNode.get();
        qNode.locked = false;
        myNode.set(myPred.get());
    }
}
